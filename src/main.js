import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Select from 'vue-select'

Vue.use(VueRouter);
Vue.component('v-select',Select)

import 'bootstrap/dist/css/bootstrap.css'
import 'jquery/dist/jquery.min'
import 'popper.js/dist/popper.min'
import 'bootstrap/dist/js/bootstrap.min'
import 'vue-select/dist/vue-select.css';

Vue.config.productionTip = false

import router from './router'


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


import VueRouter from 'vue-router'

import PostsIndex from './components/posts/Index'
import PostsCreate from './components/posts/Create'
import PostsEdit from './components/posts/Edit'

const router = new VueRouter({
    routes: [
      {
        path: '/',
        name: 'posts',
        component: PostsIndex
      },
      {
        path: '/create',
        name: 'create',
        component: PostsCreate
      },
      {
        path: '/edit/:id',
        name: 'edit',
        component: PostsEdit
      }
    ],
    mode: 'history'
})

export default router
  
